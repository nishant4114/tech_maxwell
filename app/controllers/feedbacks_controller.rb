class FeedbacksController < ApplicationController

  def new
  @feedback = Feedback.new
  end

  def create

    @feedback = Feedback.create(feedback_params)
    if @feedback.save
      redirect_to root_path
    end
  end

  private

  def feedback_params
    params.require(:feedback).permit(:name, :email, :contact_no, :description)
  end

end
